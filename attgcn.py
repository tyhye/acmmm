#!/usr/bin/env python 
# -*- code:utf-8 -*- 
'''
 @Author: tyhye.wang 
 @Date: 2018-08-01 15:47:47 
 @Last Modified by:   tyhye.wang 
 @Last Modified time: 2018-08-01 15:47:47 
'''

import os
from docopt import docopt
from easydict import EasyDict as edict
from tqdm import tqdm

from experiment.attgcn_process import train_attgcnnetwork

docstr = """ReID GCN Attribute Train Script.

Usage: 
   base.py [options]

Options:
    -h, --help                  Print this message
    --withvisdom                If the log with visdom
    --basenet=<str>             basenet type string [default: resnet50]
    --pretrained                If the model not pretrained by ImageNet.

    --train_list=<str>          Train files list txt [default: datas/Market1501/dataset/train_watt.txt]
    --train_data_root=<str>     Train sketch images path prefix [default: datas/Market1501/dataset/]
    --query_list=<str>          Query files list txt [default: datas/Market1501/dataset/query.txt]
    --gallery_list=<str>        Gallery files list txt [default: datas/Market1501/dataset/gallery.txt]
    --val_data_root=<str>       Val sketch images path prefix [default: datas/Market1501/dataset/]

    --classes_num=<int>         Number of Classes [default: [751,2,2,2,2,2,2,8,9]]
    --last_stride=<int>         Stride for the last block of the base model [default: 1]
    --batch_size=<int>          Batchsize [default: 32]
    --resize_size=<tuple>       Image reiszed size [default: [384, 128]]
    --crop_size=<tuple>         Image crop size [default: None]

    --isflip                    If average filpped images features
    --fea_norm                  If normlise the feature when val

    --learning_rate=<float>     Learning Rate [default: 1e-1]
    --base_lr_scale=<float>     Base block learning rate scale [default: 1e-1]
    --weight_decay=<float>      Weight decay for model [default: 5e-4]
    --optim=<str>               Optimizer Type [default: SGD]
    
    --lr_policy=<str>           Learning rate policy [default: multistep]
    --poly_power=<float>        Power for the poly policy [default: 0.9]    
    --lr_update_steps=<list>    Step stone for multistep policy [default: [40,]]
    
    --val_epochs=<int>          Val Epoch stone [default: 5]
    --snap_epochs=<int>         Snap iter stone [default: 5]
    --max_epochs=<int>          Max Train iters [default: 60]
    --snap_dir=<str>            Dir for snap state dicts [default: saved/]

    --device=<str>              Device for running the model [default: cuda:0]
    --dataparallel              If use DataParallel.
"""

def main():
    args = docopt(docstr, version="v0.1")
    # print(args)
    cfg = edict()

    cfg.train_list = args["--train_list"]
    cfg.train_data_root = args["--train_data_root"]
    cfg.query_list = args["--query_list"]
    cfg.gallery_list = args["--gallery_list"]
    cfg.val_data_root = args["--val_data_root"]
    
    cfg.classes_num = eval(args["--classes_num"])
    cfg.last_stride = int(args["--last_stride"])
    cfg.batch_size = int(args["--batch_size"])
    cfg.resize_size = eval(args["--resize_size"])
    cfg.crop_size = eval(args["--crop_size"])
    cfg.isflip = args["--isflip"]
    cfg.fea_norm = args["--fea_norm"]
    
    cfg.learning_rate = float(args["--learning_rate"])
    cfg.base_lr_scale = float(args["--base_lr_scale"])
    cfg.weight_decay = float(args["--weight_decay"])
    cfg.optim = args["--optim"]
    cfg.lr_police = args["--lr_policy"]
    if cfg.lr_police == "poly":
        cfg.poly_power = float(args["--poly_power"])
    elif cfg.lr_police == "multistep":
        cfg.lr_update_steps = eval(args["--lr_update_steps"])
        
    cfg.max_epochs = int(args["--max_epochs"])
    cfg.val_epochs = int(args["--val_epochs"])
    cfg.snap_epochs = int(args["--snap_epochs"])
    cfg.snap_dir = args["--snap_dir"]
    if not os.path.exists(cfg.snap_dir):
        os.makedirs(cfg.snap_dir)
    
    cfg.device = args["--device"]
    cfg.dataparallel = args["--dataparallel"]

    cfg.withvisdom = args["--withvisdom"]

    cfg.basenet = args["--basenet"]
    cfg.pretrained = args["--pretrained"]
    
    print(cfg)
    train_attgcnnetwork(cfg)

if __name__ == "__main__":
    import torch
    torch.multiprocessing.set_sharing_strategy('file_system')
    main()