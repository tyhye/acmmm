# -*- coding: utf-8 -*-

import sys
sys.path.insert(0, '.')

import argparse
import os

import random
from scipy import io
import numpy as np

NORMAL_KEYS = ['age', 'backpack', 'bag', 'handbag', 'clothes', 'down', 'up', 'hair', 'hat', 'gender']
UP_COLOR_KEYS = ['upblack', 'upblue', 'upgreen', 'upgray', 'uppurple', 'upred', 'upwhite', 'upyellow']
DOWN_COLOR_KEYS = ['downblack', 'downblue', 'downbrown', 'downgray', 'downgreen', 'downpink', 'downpurple', 'downwhite', 'downyellow']

FINAL_KEYS = ['gender','up','hat','backpack','bag','handbag','up_colors','down_colors']

class Config(object):
    
    def __init__(self):
        parser = argparse.ArgumentParser()
        # parser.add_argument('--dataset_root', type=str, default='/home/sll2/Downloads/Re-ID/Market-1501-v15.09.15/')
        parser.add_argument('--trainval_file_path', type=str, default='D:/DEVs/Market-1501/dataset/train.txt')
        parser.add_argument('--trainval_att_file_path', type=str, default='D:/DEVs/Market-1501/dataset/train_watt.txt')
        parser.add_argument('--attribute_root', type=str, default='D:/DEVs/Market-1501')
        # parser.add_argument('--id_finished', type=bool, default = False)
        # parser.add_argument('--attr_finished', type=bool, default = False)
        # query | bounding_box_train | bounding_box_test | 
        args = parser.parse_args()
        
        self.trainval_file_path = args.trainval_file_path
        self.trainval_att_file_path = args.trainval_att_file_path
        self.attribute_root = args.attribute_root
        # self.id_finished = args.id_finished
        # self.attr_finished = args.attr_finished

#GET_ID FROM IMG_PATH
def get_id(paths):
    camera_id = []
    labels = []
    for file_path in paths:
        if not file_path.strip()[-3:] == 'jpg':
                continue
        filename = file_path.split('/')[-1]
        label, cam, _, _ = filename.split('_')
        label = int(label)
        labels.append(int(label))
        
        camera = filename.split('c')[1]
        camera_id.append(int(camera[0]))
    return camera_id, labels

# GET_ATTRIBUTE_RECORD
def get_attribute_record(path):
    market_attribute = io.loadmat(path)['market_attribute']
    # I don't know why so much zeros 
    trainval_attr = market_attribute['train'][0][0][0][0]
    test_attr = market_attribute['test'][0][0][0][0]
    trainval_record = {}
    test_record = {}
    for key in NORMAL_KEYS:
        trainval_record[key] = (trainval_attr[key][0]-1).tolist()
        test_record[key] = (test_attr[key][0]-1).tolist()
    
    trainval_colors = np.int32(np.zeros_like(trainval_attr['age']))-1
    test_colors = np.zeros_like(test_attr['age'])-1
    for idx, key in enumerate(DOWN_COLOR_KEYS):
        trainval_colors = trainval_colors + (idx+1) * (trainval_attr[key][0] - 1)
        test_colors = test_colors + (idx+1) * (test_attr[key][0] - 1)
    trainval_record['down_colors'] = trainval_colors.tolist()[0]
    test_record['down_colors'] = test_colors.tolist()[0]
    
    trainval_colors = np.int32(np.zeros_like(trainval_attr['age']))-1
    test_colors = np.int32(np.zeros_like(test_attr['age']))-1
    for idx, key in enumerate(UP_COLOR_KEYS):
        trainval_colors = trainval_colors + (idx+1) * (trainval_attr[key][0] - 1)
        test_colors = test_colors + (idx+1) * (test_attr[key][0] - 1)
    trainval_record['up_colors'] = trainval_colors.tolist()[0]
    test_record['up_colors'] = test_colors.tolist()[0]
    
    return trainval_record, test_record

def main():
    cfg = Config()
    
    # ==============  Load TrainVal ID File ============
    trainval_file_name = cfg.trainval_file_path
    trainval_list = []
    with open(trainval_file_name,'r') as trainval_file_name:
        for line in trainval_file_name.readlines():
            filename, ID = line.strip().split(" ")
            ID = int(ID)
            trainval_list.append((filename, ID))
    
    # =============   Load Attribute Record ============
    attribute_path = 'market_attribute.mat'
    # get attribute record
    trainval_record, test_record = get_attribute_record(cfg.attribute_root+'/'+attribute_path) 

    # =============  Mix ID & Attribute ================
    trainval_att_list = []
    for item in trainval_list:
        index = item[1]
        att_item = [item[0], item[1]]
        for key in FINAL_KEYS:
            # print(att_item)
            # print(trainval_record)#[index]
            att_item.append(trainval_record[key][index])
        trainval_att_list.append(tuple(att_item))

    # =============  Output ID & Attribute File =======
    trainval_att_file_path = cfg.trainval_att_file_path
    with open(trainval_att_file_path,"w") as trainval_att_file:
        for item in trainval_att_list:
            trainval_att_file.write(" ".join(str(it) for it in item))
            trainval_att_file.write("\n")

    # # =================== MK ID FILE ===================
    # if not cfg.id_finished:
    #     trainval_path = 'bounding_box_train'
    #     query_path = 'query'
    #     gallery_path = 'bounding_box_test' 
        
    #     trainval_txt = cfg.dataset_root + '/trainval.txt'
    #     trainval_file = open(trainval_txt, 'w')
    #     for (_, _, files) in os.walk(cfg.dataset_root+trainval_path):
    #         files.sort()
    #         for name in files:
    #             if not name.strip()[-3:] == 'jpg':
    #                 continue
    #             trainval_file.write('{}/{}\n'.format(trainval_path, name))
    #     trainval_file.close()
        
    #     query_txt = cfg.dataset_root + '/query.txt'
    #     query_file = open(query_txt, 'w')
    #     for (_, _, files) in os.walk(cfg.dataset_root+query_path):
    #         files.sort()
    #         for name in files:
    #             if not name.strip()[-3:] == 'jpg':
    #                 continue
    #             query_file.write('{}/{}\n'.format(query_path, name))
    #     query_file.close()
        
    #     gallery_txt = cfg.dataset_root + '/gallery.txt'
    #     gallery_file = open(gallery_txt, 'w')
    #     for (_, _, files) in os.walk(cfg.dataset_root+gallery_path):
    #         files.sort()
    #         for name in files:
    #             if not name.strip()[-3:] == 'jpg':
    #                 continue
    #             gallery_file.write('{}/{}\n'.format(gallery_path, name))
    #     gallery_file.close()
        
    #     trainval_file = open(trainval_txt, 'r')
    #     trainval_files = trainval_file.readlines()
    #     trainval_file.close()
    #     camera_id, labels = get_id(trainval_files)
        
    #     # count label nums
    #     labelnums = {}
    #     for label  in labels:
    #         if label in labelnums:
    #             labelnums[label] += 1
    #         else:
    #             labelnums[label] = 1
    #     no_repeat_labels = list(set(labels))
    #     no_repeat_labels.sort()
    #     # check nums less than 4 to val dataset
    #     val_labels = []
    #     remain_labels = []
    #     for label in no_repeat_labels:
    #         if labelnums[label] < 4:
    #             val_labels.append(label)
    #         else:
    #             remain_labels.append(label)
    #     if len(val_labels) < 100:
    #         val_labels.extend(random.sample(remain_labels, 100-len(val_labels)))
    #     train_labels = []
    #     for label in no_repeat_labels:
    #         if label not in val_labels:
    #             train_labels.append(label)
        
    #     train_txt = cfg.dataset_root + '/train.txt'
    #     train_file = open(train_txt, 'w')
    #     val_txt = cfg.dataset_root + '/val.txt'
    #     val_file = open(val_txt, 'w')
    #     for label, filename in zip(labels, trainval_files):
    #         if label in train_labels:
    #             train_file.write('{} {}\n'.format(filename.strip(), train_labels.index(label)))
    #         if label in val_labels:
    #             val_file.write('{}\n'.format(filename.strip()))
    #     train_file.close()
    #     val_file.close()
    
    # # =================== MK ATTR FILES ==========================
    # if not cfg.attr_finished:
    #     attribute_path = 'market_attribute.mat'
    
    #     # get attribute record
    #     trainval_record, test_record = get_attribute_record(cfg.attribute_root+'/'+attribute_path)    
    #     trainval_txt = cfg.dataset_root + '/trainval.txt'
        
    #     # make trainval attribute record file
    #     # Format: ID attr1 att2 ...
    #     trainval_file = open(trainval_txt, 'r')
    #     trainval_files = trainval_file.readlines()
    #     trainval_file.close()
    #     camera_id, trainlabels = get_id(trainval_files)
    #     trainlabels = list(set(trainlabels))
    #     trainlabels.sort()
    #     trainval_attr_re_txt = cfg.attribute_root + '/trainval_att_record.txt'
    #     with open(trainval_attr_re_txt, 'w') as fattr:
    #         for idx, label in enumerate(trainlabels):
    #             line = '%d' % label
    #             for key in trainval_record:
    #                 line += ' %d'% trainval_record[key][idx]
    #             fattr.write('{}\n'.format(line))
        
    #     # make test attribute record file
    #     # Format: ID att1 att2
    #     gallery_txt = cfg.dataset_root + '/gallery.txt'
    #     gallery_file = open(gallery_txt, 'r')
    #     gallery_files = gallery_file.readlines()
    #     gallery_file.close()
    #     camera_id, gallerylabels = get_id(gallery_files)
    #     gallerylabels = list(set(gallerylabels))
    #     gallerylabels.remove(-1)
    #     gallerylabels.remove(0)
    #     gallerylabels.sort()
    #     gallery_attr_re_txt = cfg.attribute_root + '/test_att_record.txt'
    #     with open(gallery_attr_re_txt, 'w') as fattr:
    #         for idx, label in enumerate(gallerylabels):
    #             line = '%d' % label
    #             for key in test_record:
    #                 line += ' %d'% test_record[key][idx]
    #             fattr.write('{}\n'.format(line))
        
    #     # make trainval attr dict
    #     trainval_attr = {}
    #     with open(trainval_attr_re_txt, 'r') as fattr:
    #         for line in fattr.readlines():
    #             attrs = line.strip().split(' ')
    #             ID = int(attrs[0])
    #             trainval_attr[ID] = [int(x) for x in attrs[1:]]
        
    #     # read trainval file 
    #     trainval_txt = cfg.dataset_root + '/trainval.txt'
    #     trainval_file = open(trainval_txt, 'r')
    #     trainval_files = trainval_file.readlines()
    #     camera_id, trainlabels = get_id(trainval_files)
        
    #     # make attr trainval dataset file 
    #     trainval_attr_txt = cfg.attribute_root + '/trainval_att.txt'
    #     with open(trainval_attr_txt, 'w') as fattr:
    #         for path, label in zip(trainval_files, trainlabels):
    #             path = path.strip()
    #             attrs = ' '.join([str(x) for x in trainval_attr[label]])
    #             fattr.write('{} {}\n'.format(path, attrs))
        
    #     # make test attr dict
    #     test_attr = {}
    #     with open(gallery_attr_re_txt, 'r') as fattr:
    #         for line in fattr.readlines():
    #             attrs = line.strip().split(' ')
    #             ID = int(attrs[0])
    #             test_attr[ID] = [int(x) for x in attrs[1:]]
        
    #     # read test file 
    #     gallery_txt = cfg.dataset_root + '/gallery.txt'
    #     gallery_file = open(gallery_txt, 'r')
    #     gallery_files = gallery_file.readlines()
    #     camera_id, gallerylabels = get_id(gallery_files)
        
    #     # make attr test dataset file 
    #     gallery_attr_txt = cfg.attribute_root + '/gallery_att.txt'
    #     with open(gallery_attr_txt, 'w') as fattr:
    #         for path, label in zip(gallery_files, gallerylabels):
    #             path = path.strip()
    #             attrs = ' '.join([str(x) for x in test_attr.setdefault(label, len(test_attr[1])*[0])])
    #             fattr.write('{} {}\n'.format(path, attrs))
        
    #     # read test file 
    #     query_txt = cfg.dataset_root + '/query.txt'
    #     query_file = open(query_txt, 'r')
    #     query_files = query_file.readlines()
    #     camera_id, querylabels = get_id(query_files)
        
    #     # make attr test dataset file 
    #     query_attr_txt = cfg.attribute_root + '/query_att.txt'
    #     with open(query_attr_txt, 'w') as fattr:
    #         for path, label in zip(query_files, querylabels):
    #             path = path.strip()
    #             attrs = ' '.join([str(x) for x in test_attr.setdefault(label, len(test_attr[1])*[0])])
    #             fattr.write('{} {}\n'.format(path, attrs))
        
    # # =================== MK TRAIN ID & ATTR FILE
    # train_txt = cfg.dataset_root + '/train.txt'
    # train_file = open(train_txt, 'r')
    # train_files = train_file.readlines()
    # train_paths = [t.strip().split(' ')[0] for t in train_files]
    # camera_id, trainlabels = get_id(train_paths)
    
    
    # # make trainval attr dict
    # trainval_attr_re_txt = cfg.attribute_root + '/trainval_att_record.txt'
    # trainval_attr = {}
    # with open(trainval_attr_re_txt, 'r') as fattr:
    #     for line in fattr.readlines():
    #         attrs = line.strip().split(' ')
    #         ID = int(attrs[0])
    #         trainval_attr[ID] = [int(x) for x in attrs[1:]]
    # attrlen = len(trainval_attr[ID])
    
    # train_id_attr_txt = cfg.dataset_root + '/train_id&attr.txt'
    # with open(train_id_attr_txt, 'w') as fattr:
    #         for path_id, label in zip(train_files, trainlabels):
    #             path_id = path_id.strip()
    #             attrs = ' '.join([str(x) for x in trainval_attr.setdefault(label, attrlen*[-1])])
    #             print('{} | {}'.format(path_id, attrs))
    #             fattr.write('{} | {}\n'.format(path_id, attrs))
    
        
if __name__ == '__main__':
    main()

