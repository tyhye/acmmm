/*
 * @Author: mikey.zhaopeng 
 * @Date: 2019-03-14 11:24:27 
 * @Last Modified by: mikey.zhaopeng
 * @Last Modified time: 2019-03-14 11:28:24
 */

import torch
from experiment.model.gcnmodel import GCNAttNetwork

from experiment.model.resnet import resnet18, resnet34, resnet50
from experiment.model.resnet import resnet101, resnet152

Net = GCNAttNetwork(basenet=resnet50, pretrained=cfg.pretrained,
                    classes_num=10, last_stride=1)
x = torch.randn(16, 3, 384, 128)

y = Net(x)
