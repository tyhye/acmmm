#!/usr/bin/env python
# -*- code:utf-8 -*-
'''
 @Author: tyhye.wang
 @Date: 2018-08-08 15:22:20
 @Last Modified by:   tyhye.wang
 @Last Modified time: 2018-08-08 15:22:20
'''

from tqdm import tqdm
import random

import torch
import torch.nn as nn
import torchnet as tnt

from .process.epochprocessor import EpochProcessor
from .dataload.dataloader import train_withatt_dataloader
from .dataload.dataloader import market1501_dataloader
from .model.gcnmodel import GCNAttNetwork
from .model.resnet import resnet18, resnet34, resnet50
from .model.resnet import resnet101, resnet152
from .meter.reidmeter import ReIDMeter


def get_params(Model, cfg):
    params = [
        {'params': Model.conv.parameters(), 'lr': cfg.base_lr_scale*cfg.learning_rate},
        {'params': Model.feature_extractors.parameters(), 'lr': cfg.learning_rate},
        {'params': Model.bf_classifiers.parameters(), 'lr': cfg.learning_rate},
        {'params': Model.GCN1.parameters(), 'lr': cfg.learning_rate},
        {'params': Model.GCN2.parameters(), 'lr': cfg.learning_rate},
        {'params': Model.classifiers.parameters(), 'lr': cfg.learning_rate}]
    return params

def get_edge_index(Dataset=None, batch_size=32):
    import numpy as np
    # edge_index = np.array(
    #        [
    #            [0,1],[0,2],[0,3],[0,4],[0,5],[0,6],
    #            [1,2],[2,3],[3,4],[4,5],[5,6]
    #        ])
    edge_index = np.array(
           [
               [0,1],[0,2],[0,3],[0,4],[0,5],[0,6],[0,7],[0,8],
               [1,7],[1,8],
               [4,5],[4,6]
           ])
    edge_index_ = np.concatenate((edge_index[:,1:2], edge_index[:,0:1]), axis=1)
    edge_index = np.concatenate((edge_index, edge_index_), axis=0)
    edge_index = edge_index.transpose(1,0)
    l = edge_index.shape[1]
    help_index = np.arange(0,batch_size)
    help_index = help_index.repeat(l)
    # help_index = help_index.reshape(-1)
    help_index = help_index * (np.max(edge_index) + 1)
    edge_index = np.tile(edge_index,(1,batch_size))
    edge_index = edge_index + help_index
    return edge_index, l


def train_attgcnnetwork(cfg, logger=print):

    Net = GCNAttNetwork(basenet=eval(cfg.basenet), pretrained=cfg.pretrained,
                      classes_num=cfg.classes_num, last_stride=cfg.last_stride)
    Net.cuda(cfg.device)
    if cfg.dataparallel:
        Net = nn.DataParallel(Net)
        Model = Net.module
    else:
        Model = Net
    print(Net)

    logger("#parameters: %d" % sum(param.numel()
                                   for param in Net.parameters()))
    params = get_params(Model, cfg)

    edge_index_full, edge_length = get_edge_index(batch_size=cfg.batch_size)
    edge_index_full = torch.tensor(edge_index_full, dtype=torch.long)
    edge_index_full = edge_index_full.cuda(cfg.device)

    from torch.optim import SGD, Adam
    from torch.optim.lr_scheduler import MultiStepLR, LambdaLR
    if cfg.optim == 'Adam':
        optimizer = Adam(params, lr=cfg.learning_rate, betas=(
            0.5, 0.999), eps=1e-12, weight_decay=cfg.weight_decay)
    if cfg.optim == 'SGD':
        optimizer = SGD(params, lr=cfg.learning_rate, momentum=0.9,
                        weight_decay=cfg.weight_decay,
                        nesterov=True)
    if cfg.lr_police == "poly":
        def lambda1(t): return (1 - float(t)/cfg.max_epcohs)**cfg.poly_power
        lr_scheduler = LambdaLR(optimizer, lambda1)
    elif cfg.lr_police == "multistep":
        lr_scheduler = MultiStepLR(
            optimizer, milestones=cfg.lr_update_steps, gamma=0.1)
    print(optimizer)
    print(lr_scheduler)

    criterion = nn.CrossEntropyLoss(ignore_index=-1)
    meter_loss = tnt.meter.AverageValueMeter()
    meter_accuracy = tnt.meter.ClassErrorMeter(accuracy=True)
    meter_reid = ReIDMeter(isnorm=cfg.fea_norm)

    save_name = "base_reid"
    if cfg.withvisdom:
        from visdom import Visdom
        from torchnet.logger import VisdomPlotLogger
        visdom = Visdom()
        port = 8097
        train_lr_logger = VisdomPlotLogger(
            'line', port=port, opts={'title': 'Learning Rate'})
        train_loss_logger = VisdomPlotLogger(
            'line', port=port, opts={'title': 'Train Loss'})
        train_accuracy_logger = VisdomPlotLogger(
            'line', port=port, opts={'title': 'Train Accuracy'})
        val_reid_logger = VisdomPlotLogger(
            'line', port=port, opts={'title': 'ReID CMC&mAP', 'legend': ["CMC1", 'mAP']})

    # ===============define states================
    #  define get_iterator
    #  mode = True is Training
    #  mode = Flase is Testing
    #  iterator in engine.train(network, iterator, maxepoch, optimizer)
    #  from PKTriDataset import PKTriDataset
    def get_iterator(mode):
        # print("get_iterator")
        # input()
        if mode:
            cfg.train_load = train_withatt_dataloader(resize_size=cfg.resize_size,
                                              crop_size=cfg.crop_size,
                                              data_root=cfg.train_data_root)
            dataset = tnt.dataset.ListDataset(cfg.train_list,
                                              load=cfg.train_load)
            return dataset.parallel(batch_size=cfg.batch_size, num_workers=8,
                             shuffle=mode, drop_last=mode)
        else:
            cfg.val_load = market1501_dataloader(resize_size=cfg.resize_size,
                                                 crop_size=cfg.crop_size,
                                                 data_root=cfg.val_data_root)
            query_dataset = tnt.dataset.ListDataset(cfg.query_list,
                                                    load=cfg.val_load)
            query_iterator = query_dataset.parallel(batch_size=cfg.batch_size,
                                                    num_workers=8,
                                                    shuffle=mode, drop_last=mode)
            gallery_dataset = tnt.dataset.ListDataset(cfg.gallery_list,
                                                      load=cfg.val_load)
            gallery_iterator = gallery_dataset.parallel(batch_size=cfg.batch_size,
                                                      num_workers=8,
                                                      shuffle=mode, drop_last=mode)

            def test_iterator():
                for data in tqdm(query_iterator, desc="query iterations", ncols=80):
                    if isinstance(data, (tuple, list)):
                        data.append('query')
                    else:
                        data = (data, 'query')
                    yield data
                for data in tqdm(gallery_iterator, desc="gallery iterations", ncols=80):
                    if isinstance(data, (tuple, list)):
                        data.append('gallery')
                    else:
                        data = (data, 'gallery')
                    yield data
            
            return test_iterator()

    # define reset_meters
    def reset_meters():
        # print("reset_meters")
        # input()
        meter_loss.reset()
        meter_accuracy.reset()
        meter_reid.reset()

    def on_start(state):
        # print("on_start")
        # input()
        if state['train']:
            state['store_iteration'] = state['iterator']

    def on_start_epoch(state):
        # print("on_start_epoch")
        # input()
        lr_scheduler.step()
        if state['train']:
            state['iterator'] = tqdm(state['store_iteration'],
                                     desc="Epoch %d Train" % (
                                         state['epoch']+1),
                                     ncols=80)

    # define sample
    def on_sample(state):
        # print("on_sample")
        # input()
        pass

    def train_process(sample):
        # print("train_process")
        # input()
        # print(sample)
        data, label = sample
        edge_index = edge_index_full[:,:(data.size(0)*edge_length)]
        data = data.cuda(cfg.device)
        label = label.cuda(cfg.device)
        optimizer.zero_grad()
        bf_IDs, _, IDs, _ = Net(data, edge_index)
        bf_losses = [criterion(item, l) for item, l in zip(bf_IDs, label)]
        losses = [criterion(item, l) for item, l in zip(IDs, label)]
        bf_loss = sum(bf_losses)/len(bf_losses)
        loss = sum(losses)/len(losses)
        loss = bf_loss + loss
        loss.backward()
        optimizer.step()
        return loss, bf_IDs[0]

    def test_process(sample):
        # print("test_sample")
        # input()
        data, _, _, _ = sample
        edge_index = edge_index_full[:,:(data.size(0)*edge_length)]
        data = data.cuda(cfg.device)
        _, bf_feas, _, feas = Net(data, edge_index)
        fea = torch.cat(bf_feas, dim=1)
        fea = fea.detach()
        if cfg.isflip:
            data = torch.flip(data, dims=[3])
            _, bf_feas_, _, feas_ = Net(data, edge_index)
            fea_ = torch.cat(bf_feas_, dim=1)
            fea_ = fea_.detach()
            fea = (fea + fea_)/2
        return _, fea

    def on_forward(state):
        # print("on_forward")
        # input()
        if state['train']:
            loss = state['loss']
            meter_loss.add(loss.detach().cpu().data)
            ID = state['output'].detach()
            label = state['sample'][1]
            meter_accuracy.add(ID, label)
        else:
            cam = state['sample'][1]
            label = state['sample'][2]
            fea_type = state['sample'][3]
            fea = state['output']
            meter_reid.add(fea, cam, label, feature_type=fea_type)

    def on_end_epoch(state):
        # print("on_end_epoch")
        # input()
        if state['train']:
            loss = meter_loss.value()[0]
            learning_rate = optimizer.param_groups[-1]['lr']
            accuracy = meter_accuracy.value()[0]
            logger('[Epoch %d] Training Loss: %.4f Learning rate: %.6f Accuracy: %.2f%%' %
                    (state['epoch'], loss, learning_rate, accuracy))
            if cfg.withvisdom:
                train_lr_logger.log(state['epoch'], learning_rate)
                train_loss_logger.log(state['epoch'], loss)
                train_accuracy_logger.log(state['epoch'], accuracy)
                visdom.save(["main"])
            reset_meters()

            if state['epoch'] % cfg.snap_epochs == 0:
                Net.eval()
                torch.save(Model.state_dict(), '%s/%s_epoch_%d.pt' %
                           (cfg.snap_dir, save_name, state['epoch']))
                Net.train()

            if state['epoch'] % cfg.val_epochs == 0:
                reset_meters()
                Net.eval()
                processor.test(test_process, get_iterator(False))
                CMC, mAP = meter_reid.value()
                logger('[Epoch %d] R1: %.2f R5: %.2f R10: %.2f mAP: %.2f' %
                       (state['epoch'], CMC[0]*100, CMC[4]*100, CMC[9]*100, mAP*100))
                if cfg.withvisdom:
                    val_reid_logger.log([state['epoch'], state['epoch']], [CMC[0]*100, mAP*100])
                    visdom.save(["main"])
                reset_meters()
                Net.train()

    def on_end(state):
        # print("on_end")
        # input()
        pass

    processor = EpochProcessor()
    processor.hooks['on_start'] = on_start
    processor.hooks['on_start_epoch'] = on_start_epoch
    processor.hooks['on_sample'] = on_sample
    processor.hooks['on_forward'] = on_forward
    processor.hooks['on_end_epoch'] = on_end_epoch
    processor.hooks['on_end'] = on_end

    processor.train(train_process, get_iterator(True), cfg.max_epochs)
