#!/usr/bin/env python
# -*- code:utf-8 -*-
'''
 @Author: tyhye.wang 
 @Date: 2018-08-30 20:11:54 
 @Last Modified by:   tyhye.wang 
 @Last Modified time: 2018-08-30 20:11:54 
'''

import torchvision.transforms as transforms

import os
import random

from PIL import Image

MEAN = [0.485, 0.456, 0.406]
STD = [0.229, 0.224, 0.225]

class train_withatt_dataloader(object):
    
    def __init__(self, resize_size=None, crop_size=None, data_root=None):
        self.data_root = data_root
        tflist = []
        if resize_size is not None:
            tflist.append(transforms.Resize(resize_size, interpolation=Image.BICUBIC))
        if crop_size is not None:
            tflist.append(transforms.Pad(10))
            tflist.append(transforms.RandomCrop(crop_size))
        tflist.extend([
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize(MEAN, STD)
        ])
        self.transform = transforms.Compose(tflist)

        # transform_train_list = [
        #     #transforms.RandomResizedCrop(size=128, scale=(0.75,1.0), ratio=(0.75,1.3333), interpolation=3), #Image.BICUBIC)
        #     transforms.Resize((256,128), interpolation=3),
        #     transforms.Pad(10),
        #     transforms.RandomCrop((256,128)),
        #     transforms.RandomHorizontalFlip(),
        #     transforms.ToTensor(),
        #     transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        # ]
        # self.transform = transforms.Compose(transform_train_list)
        
    def __call__(self, line):
        items = line.strip().split()
        filename = items[0]
        IDWATT = items[1:]

        if self.data_root is not None:
            filename = os.path.join(self.data_root, filename)
        IDWATT = [int(iwa) for iwa in IDWATT]
        ID, gender, up, hat, BP, Bag, HB, UC, DC = IDWATT
        img = Image.open(filename).convert("RGB")
        img = self.transform(img)
        return img, ID, gender, up, hat, BP, Bag, HB, UC, DC

class train_dataloader(object):

    def __init__(self, resize_size=None, crop_size=None, data_root=None):
        self.data_root = data_root
        tflist = []
        if resize_size is not None:
            tflist.append(transforms.Resize(resize_size, interpolation=Image.BICUBIC))
        if crop_size is not None:
            tflist.append(transforms.Pad(10))
            tflist.append(transforms.RandomCrop(crop_size))
        tflist.extend([
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize(MEAN, STD)
        ])
        self.transform = transforms.Compose(tflist)

        # transform_train_list = [
        #     #transforms.RandomResizedCrop(size=128, scale=(0.75,1.0), ratio=(0.75,1.3333), interpolation=3), #Image.BICUBIC)
        #     transforms.Resize((256,128), interpolation=3),
        #     transforms.Pad(10),
        #     transforms.RandomCrop((256,128)),
        #     transforms.RandomHorizontalFlip(),
        #     transforms.ToTensor(),
        #     transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        # ]
        # self.transform = transforms.Compose(transform_train_list)
        
    def __call__(self, line):
        filename, ID = line.strip().split()
        if self.data_root is not None:
            filename = os.path.join(self.data_root, filename)
        ID = int(ID)
        img = Image.open(filename).convert("RGB")
        img = self.transform(img)
        return img, ID

class market1501_dataloader(object):
    
    def __init__(self, resize_size=None, crop_size=None, data_root=None):
        self.data_root = data_root
        tflist = []
        if resize_size is not None:
            tflist.append(transforms.Resize(resize_size, interpolation=Image.BICUBIC))
        if crop_size is not None:
            tflist.append(transforms.CenterCrop(crop_size))
        tflist.extend([
            transforms.ToTensor(),
            transforms.Normalize(MEAN, STD)
        ])
        self.transform = transforms.Compose(tflist)

    def get_label_cam(self, file_path):
        filename = file_path.split('/')[-1]
        cam = int(filename.split('c')[-1].split('s')[0])
        label = int(filename.split('_')[0])
        return cam, label

    def __call__(self, line):
        filename = line.strip()
        if self.data_root is not None:
            filename = os.path.join(self.data_root, filename)
        img = Image.open(filename).convert("RGB")
        img = self.transform(img)
        cam, label = self.get_label_cam(filename)
        return img, cam, label