#!/usr/bin/env python
# -*- code:utf-8 -*-
'''
 @Author: tyhye.wang 
 @Date: 2018-09-10 08:50:19 
 @Last Modified by:   tyhye.wang 
 @Last Modified time: 2018-09-10 08:50:19 
'''

import torch
import torch.nn as nn
import torch.nn.functional as F

from .weight_init import weights_init_kaiming, weights_init_classifier


class BaseNetwork(nn.Module):

    def __init__(self, basenet, pretrained=True,
                 fea_channels=512, classes_num=751, last_stride=2):
        self.inplanes = 64
        super(BaseNetwork, self).__init__()

        self.conv = basenet(pretrained=pretrained, last_stride=last_stride)
        self.pool = nn.AdaptiveAvgPool2d((1, 1))
        self.feature = nn.Sequential(
            # nn.BatchNorm1d(self.conv.out_channels),
            nn.Linear(self.conv.out_channels, fea_channels),
        )
        self.classifier = nn.Sequential(
            nn.BatchNorm1d(fea_channels),
            nn.LeakyReLU(negative_slope=0.1, inplace=True),
            nn.Linear(fea_channels, classes_num, bias=False)
        )
        # self.feature = nn.Sequential(
        #     nn.Linear(2048, fea_channels)
        #     nn.BatchNorm1d(self.conv.out_channels)
        # )
        # self.classifier = nn.Sequential(
        #     nn.Linear(self.conv.out_channels, classes_num)
        # )

        self.feature.apply(weights_init_kaiming)
        self.classifier.apply(weights_init_classifier)

    def forward(self, x):
        x = self.conv(x)
        x = self.pool(x)
        x = x.view(x.size(0), -1)
        fea = self.feature(x)
        ID = self.classifier(fea)
        return ID, fea


class AdaptivePartitionNetwork(nn.Module):

    def __init__(self, basenet, pretrained=True, classes_num=751, last_stride=2):
        self.inplanes = 64
        super(AdaptivePartitionNetwork, self).__init__()
        self.conv = basenet(pretrained=pretrained, last_stride=last_stride)

        self.features = nn.ModuleList()
        self.classifiers = nn.ModuleList()
        self.features.apply(weights_init_kaiming)
        self.classifiers.apply(weights_init_classifier)

    def _make_layer(self, block, planes, blocks, stride=1):
        pass
        # return nn.Sequential(*layers)

    def forward(self, x):
        x = self.conv(x)
        feas = [feature(x) for feature in self.features]
        IDs = [classifier(fea)
               for classifier, fea in zip(self.classifiers, feas)]
        return IDs, feas
