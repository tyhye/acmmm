#!/usr/bin/env python
# -*- code:utf-8 -*-
'''
 @Author: tyhye.wang
 @Date: 2018-10-09 19:27:49
 @Last Modified by:   tyhye.wang
 @Last Modified time: 2018-10-09 19:27:49
'''

import math
import threading

BOUND_2468 = {
    2: {'L': [(0, 14), (9, 23)],
        'M': [(0, 11), (12, 23)],
        'S': [(0, 8), (15, 23)]},
    4: {'L': [(0, 6), (5, 12), (11, 18), (17, 23)],
        'M': [(0, 5), (6, 11), (12, 17), (18, 23)],
        'S': [(0, 4), (7, 10), (13, 16), (19, 23)]},
    6: {'L': [(0, 4), (3, 8), (7, 12), (11, 16), (15, 20), (19, 23)],
        'M': [(0, 3), (4, 7), (8, 11), (12, 15), (16, 19), (20, 23)],
        'S': [(1, 2), (5, 6), (9, 10), (13, 14), (17, 18), (21, 22)]},
    8: {'L': [(0, 3), (2, 6), (5, 9), (8, 12), (11, 15), (14, 18), (17, 21), (20, 23)],
        'M': [(0, 2), (3, 5), (6, 8), (9, 11), (12, 14), (15, 17), (18, 20), (21, 23)],
        'S': [(1, 1), (4, 4), (7, 7), (10, 10), (13, 13), (16, 16), (19, 19), (22, 22)]},
}

class ScheduleHelper(object):
    _instance_lock = threading.Lock()

    def __init__(self):
        self.bounds = {}
        for pn in BOUND_2468:
            self.bounds[pn] = []
            for x in range(pn):
                self.bounds[pn].append({c: BOUND_2468[pn][c][x] for c in "LMS"})
        self.allbounds = []
        for pn in self.bounds:
            for items in self.bounds[pn]:
                for c in items:
                    self.allbounds.append(items[c])
    
    def get_bounds(self, sch):
        return [self.bounds[len(sch)][idx][c] for idx, c in enumerate(sch)]

    def __str__(self):
        return self.allbounds.__str__()

    def __new__(cls, *args, **kwargs):
        if not hasattr(ScheduleHelper, "_instance"):
            with ScheduleHelper._instance_lock:
                if not hasattr(ScheduleHelper, "_instance"):
                    ScheduleHelper._instance = object.__new__(cls)  
        return ScheduleHelper._instance

# {
#     2: [{'L': (0, 14),  'M': (0, 11),  'S': (0, 8)},
#         {'L': (9, 23),  'M': (12, 23), 'S': (15, 23)}],
#     4: [{'L': (0, 6),   'M': (0, 5),   'S': (0, 4)},
#         {'L': (5, 12),  'M': (6, 11),  'S': (7, 10)},
#         {'L': (11, 18), 'M': (12, 17), 'S': (13, 16)},
#         {'L': (17, 23), 'M': (18, 23), 'S': (19, 23)}],
#     6: [{'L': (0, 4),   'M': (0, 3),   'S': (1, 2)},
#         {'L': (3, 8),   'M': (4, 7),   'S': (5, 6)},
#         {'L': (7, 12),  'M': (8, 11),  'S': (9, 10)},
#         {'L': (11, 16), 'M': (12, 15), 'S': (13, 14)},
#         {'L': (15, 20), 'M': (16, 19), 'S': (17, 18)},
#         {'L': (19, 23), 'M': (20, 23), 'S': (21, 22)}],
#     8: [{'L': (0, 3),   'M': (0, 2),   'S': (1, 1)},
#         {'L': (2, 6),   'M': (3, 5),   'S': (4, 4)},
#         {'L': (5, 9),   'M': (6, 8),   'S': (7, 7)},
#         {'L': (8, 12),  'M': (9, 11),  'S': (10, 10)},
#         {'L': (11, 15), 'M': (12, 14), 'S': (13, 13)},
#         {'L': (14, 18), 'M': (15, 17), 'S': (16, 16)},
#         {'L': (17, 21), 'M': (18, 20), 'S': (19, 19)},
#         {'L': (20, 23), 'M': (21, 23), 'S': (22, 22)}]
# }                                                                                                     'M': (3, 5), 'S': (4, 4)}, {'L': (5, 9), 'M': (6, 8), 'S': (7, 7)}, {'L': (8, 12), 'M': (9, 11), 'S': (10, 10)}, {'L': (11, 15), 'M': (12, 14), 'S': (13, 13)}, {'L': (14, 18), 'M': (15, 17), 'S': (16, 16)}, {'L': (17, 21), 'M': (18, 20), 'S': (19, 19)}, {'L': (20, 23), 'M': (21, 23), 'S': (22, 22)}]}
# tmp = {2:[], 4:[], 6:[], 8:[]}
# for key in tmp:
#     for x in range(key):
#         tmp[key].append({c: BOUND_2468[key][c][x] for c in "LMS"})

# print(tmp)