import torch
import torch.nn as nn
import torch.nn.functional as F
from torch_geometric.nn import GCNConv

from .weight_init import weights_init_kaiming, weights_init_classifier

class GCNAttNetwork(nn.Module):
    
    def __init__(self, basenet, pretrained=True,
                 fea_channels=512, classes_num=(751,2,2,2,2,2,2,8,9), last_stride=2):
        self.inplanes = 64
        super(GCNAttNetwork, self).__init__()

        self.conv = basenet(pretrained=pretrained, last_stride=last_stride)
        self.globalpool = nn.AdaptiveAvgPool2d((1,1))

        self.feature_extractors = nn.ModuleList(
            [
                nn.Sequential(
                    nn.Linear(self.conv.out_channels, fea_channels),
                    nn.BatchNorm1d(fea_channels),
                    nn.ReLU(inplace=True)
                ) for _ in classes_num]
        )
        
        self.bf_classifiers = nn.ModuleList(
            [nn.Linear(fea_channels, cn) for cn in classes_num]
        )

        self.GCN1 = GCNConv(fea_channels, fea_channels)
        self.GCN2 = GCNConv(fea_channels, fea_channels)
        
        self.classifiers = nn.ModuleList(
            [nn.Linear(fea_channels, cn, bias=False) for cn in classes_num]
        )

        #self.feature.apply(weights_init_kaiming)
        self.feature_extractors.apply(weights_init_kaiming)
        self.GCN1.apply(weights_init_kaiming)
        self.GCN2.apply(weights_init_kaiming)
        self.bf_classifiers.apply(weights_init_classifier)
        self.classifiers.apply(weights_init_classifier)
    
    def forward(self, x, edge_index):
        x = self.conv(x)
        x = self.globalpool(x)
        x = x.view(x.size(0),-1)
        bf_feas = [feature_extractor(x) for feature_extractor in self.feature_extractors]
        bf_IDs = [bf_classifier(bf_fea) for bf_fea, bf_classifier in zip(bf_feas, self.bf_classifiers)]

        bf_fea = torch.stack(bf_feas, dim=1)
        node_num = bf_fea.size(1)
        bf_fea = bf_fea.view(bf_fea.size(0)*bf_fea.size(1), bf_fea.size(2))
        
        x = self.GCN1(bf_fea, edge_index)
        x = F.relu(x)
        x = self.GCN2(x, edge_index)

        x = x.view(x, node_num, x.size(1))
        xs = x.split(1, dim=1)
        feas = [x.view(x.size(0), x.size(2)) for x in xs]
        IDs = [classifier(fea) for fea, classifier in zip(feas, self.classifiers)]
        return bf_IDs, bf_feas, IDs, feas

class GCNPartNetwork(nn.Module):
    
    def __init__(self, basenet, pretrained=True,
                 fea_channels=512, classes_num=751, last_stride=2):
        self.inplanes = 64
        super(GCNPartNetwork, self).__init__()

        self.conv = basenet(pretrained=pretrained, last_stride=last_stride)
        self.globalpool = nn.AdaptiveAvgPool2d((1,1))
        self.partpool = nn.AdaptiveMaxPool2d((6, 1))
        
        self.edge_index = torch.tensor(
           [
               [0,0,0,0,0,0,1,2,3,4,5,6,1,2,2,3,3,4,4,5,5,6],
               [1,2,3,4,5,6,0,0,0,0,0,0,2,1,3,2,4,3,5,4,6,5]
           ], dtype=torch.long)
        
        self.GCN1 = GCNConv(self.conv.out_channels, fea_channels)
        self.GCN2 = GCNConv(fea_channels, fea_channels)
        
        self.classifiers = nn.ModuleList(
            [nn.Linear(fea_channels, classes_num, bias=False) for _ in range(7)]
        )

        #self.feature.apply(weights_init_kaiming)
        self.GCN1.apply(weights_init_kaiming)
        self.GCN2.apply(weights_init_kaiming)
        self.classifiers.apply(weights_init_classifier)
    
    def forward(self, x):
        x = self.conv(x)
        gx = self.globalpool(x)
        pxs = self.partpool(x)
        
        x = torch.cat((gx, pxs),dim=2)
        x = torch.transpose(x,1,2)
        b = x.size(0)
        l = self.edge_index.size(1)
        edge_index = self.edge_index.repeat(1,b)
        index = torch.range(0, b-1, dtype=torch.long).view(-1,1).repeat(1, l).view(-1)
        index = index * 7
        edge_index = edge_index  + index
        edge_index = edge_index.detach()
        edge_index = edge_index.to(x.device)
        x = x.contiguous().view(x.size(0)*x.size(1), x.size(2))
        
        x = self.GCN1(x, edge_index)
        x = F.relu(x)
        x = self.GCN2(x, edge_index)

        x = x.view(b, 7, x.size(1))
        xs = x.split(1, dim=1)
        feas = [x.view(x.size(0), x.size(2)) for x in xs]
        IDs = [classifier(fea) for fea, classifier in zip(feas, self.classifiers)]
        return IDs, feas
        
class BaseNetwork(nn.Module):

    def __init__(self, basenet, pretrained=True,
                 fea_channels=512, classes_num=751, last_stride=2):
        self.inplanes = 64
        super(BaseNetwork, self).__init__()

        self.conv = basenet(pretrained=pretrained, last_stride=last_stride)
        self.pool = nn.AdaptiveAvgPool2d((1, 1))
        self.feature = nn.Sequential(
            # nn.BatchNorm1d(self.conv.out_channels),
            nn.Linear(self.conv.out_channels, fea_channels),
        )
        self.classifier = nn.Sequential(
            nn.BatchNorm1d(fea_channels),
            nn.LeakyReLU(negative_slope=0.1, inplace=True),
            nn.Linear(fea_channels, classes_num, bias=False)
        )
        # self.feature = nn.Sequential(
        #     nn.Linear(2048, fea_channels)
        #     nn.BatchNorm1d(self.conv.out_channels)
        # )
        # self.classifier = nn.Sequential(
        #     nn.Linear(self.conv.out_channels, classes_num)
        # )

        self.feature.apply(weights_init_kaiming)
        self.classifier.apply(weights_init_classifier)

    def forward(self, x):
        x = self.conv(x)
        x = self.pool(x)
        x = x.view(x.size(0), -1)
        fea = self.feature(x)
        ID = self.classifier(fea)
        return ID, fea

