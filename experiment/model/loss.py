#!/usr/bin/env python 
# -*- code:utf-8 -*- 
'''
 @Author: tyhye.wang 
 @Date: 2018-10-15 11:06:06 
 @Last Modified by:   tyhye.wang 
 @Last Modified time: 2018-10-15 11:06:06 
'''

import torch
import torch.nn as nn
import torch.nn.functional as F

from .weight_init import weights_init_kaiming, weights_init_classifier
from .schedule import ScheduleHelper

class myloss(nn.Module):

    def __init__(self):
        super(myloss, self).__init__()
    
    def forward(self, size, schedule, output, label):
        
