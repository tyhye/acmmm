#!/usr/bin/env python
# -*- code:utf-8 -*-
'''
 @Author: tyhye.wang 
 @Date: 2018-09-10 08:50:19 
 @Last Modified by:   tyhye.wang 
 @Last Modified time: 2018-09-10 08:50:19 
'''

import torch
import torch.nn as nn
import torch.nn.functional as F

from .weight_init import weights_init_kaiming, weights_init_classifier
from .schedule import ScheduleHelper

sizetypes = ["L", "M", "S"]

class APNetwork(nn.Module):

    def __init__(self, basenet, pretrained=True,
                 fea_channels=512, classes_num=751,
                 part_nums=[2,4,6,8], last_stride=2):
        super(APNetwork, self).__init__()
        
        self.inplanes = 64
        self.part_nums = part_nums
        self.conv = basenet(pretrained=pretrained, last_stride=last_stride)

        self.features = nn.ModuleDict()
        for part_num in part_nums:
            self.features[part_num] = nn.ModuleList(
                [nn.Sequential(
                    nn.Linear(self.conv.out_channels, fea_channels),
                    # nn.BatchNorm1d(fea_channels)
                ) for _ in range(part_num)]
            )

        self.classifiers = nn.ModuleDict()
        for part_num in part_nums:
            self.classifiers[part_num] = nn.ModuleList(
                [nn.Sequential(
                    # nn.Linear(self.conv.out_channels, fea_channels),
                    nn.BatchNorm1d(fea_channels),
                    nn.Linear(fea_channels, classes_num)
                ) for _ in range(part_num)]
            )

        self.part_designer = nn.Sequential(
            nn.Linear(self.conv.out_channels, len(part_num))
        )

        self.size_designers = nn.ModuleDict()
        for part_num in part_nums:
            self.size_designers[part_num] = nn.ModuleList(
                [nn.Sequential(
                    nn.Linear(self.conv.out_channles, 3)
                ) for _ in range(part_num)]
            )

        self.features.apply(weights_init_kaiming)
        self.classifiers.apply(weights_init_classifier)
        self.part_designer.apply(weights_init_kaiming)
        self.size_designers.apply(weights_init_classifier)

        self.sher = ScheduleHelper()

    def forward(self, x):
        x = self.conv(x)
        globalx = F.adaptive_avg_pool2d(x, 1).view(x.size(0), -1)
        
        pn = self.part_designer(globalx).argmax(dim=1)
        PN = [self.part_nums[p] for p in pn]
        sizes = {}
        for part_num in self.part_nums:
            size = [model(globalx).argmax(dim=1) for model in self.size_designers[part_num]]
            sizes[part_num] = []
            for idx in range(len(PN)):
                sizes[part_num].append(''.join([sizetypes[s[idx]] for s in size]))
        
        xs = [F.adaptive_avg_pool2d(x[:, :, upbound:downbound+1], 1) for (upbound, downbound) in self.sher.allbounds]
        xs = [x.view(x.size(0), -1) for x in xs]

        feas = []
        for idx in range(x.size(0)):
            for 



        

        
        
        

