#!/usr/bin/env python
# -*- code:utf-8 -*-
'''
 @Author: tyhye.wang 
 @Date: 2018-06-16 08:05:43 
 @Last Modified by:   tyhye.wang 
 @Last Modified time: 2018-06-16 08:05:43 
 
 One metric object extend from the Metric.
 This metric is designed for person re-id retrival
'''

import numpy as np
import math
import torch
from tqdm import tqdm

class ReIDMeter(object):
    
    def __init__(self, isnorm=False):
        self.isnorm = isnorm
        self.reset()

    def reset(self):
        '''Resets the meter to default settings.'''
        self.query_features = []
        self.query_labels = []
        self.query_cams = []
        self.gallery_features = []
        self.gallery_labels = []
        self.gallery_cams = []
        self.reidmetric = None
    
    def add(self, features, cams, labels, feature_type='query'):
        self.reidmetric = None
        if feature_type == 'query':
            features_list = self.query_features
            cams_list = self.query_cams
            labels_list = self.query_labels
        else:
            features_list = self.gallery_features
            cams_list = self.gallery_cams
            labels_list = self.gallery_labels
        if torch.is_tensor(features):
            features = features.cpu().numpy()
        if torch.is_tensor(cams):
            cams = cams.cpu().numpy()
        if torch.is_tensor(labels):
            labels = labels.cpu().numpy()
        if self.isnorm:
            fnorm = np.sqrt(np.sum(features ** 2, axis=1, keepdims=True))
            features = features / fnorm
        features_list.append(features)
        cams_list.append(cams)
        labels_list.append(labels)

    def value(self):
        if self.reidmetric is not None:
            return self.reidmetric
        else:
            query_features = np.concatenate(self.query_features, axis=0)
            query_cams = np.concatenate(self.query_cams, axis=0)
            query_labels = np.concatenate(self.query_labels, axis=0)
            gallery_features = np.concatenate(self.gallery_features, axis=0)
            gallery_cams = np.concatenate(self.gallery_cams, axis=0)
            gallery_labels = np.concatenate(self.gallery_labels, axis=0)
            query_size = len(query_labels)

            CMC = np.zeros(len(gallery_labels)).astype('int32')
            ap = 0.0
            # distance = np.matmul(query_features, gallery_features.T)
            for i in tqdm(range(query_size), ncols=80, desc="Computing CMC&mAP"):
                ap_tmp, CMC_tmp = evaluate_oneitem(query_features[i],
                                                   query_labels[i],
                                                   query_cams[i],
                                                   gallery_features,
                                                   gallery_labels,
                                                   gallery_cams)
                if CMC_tmp[0] == -1:
                    continue
                CMC = CMC + CMC_tmp
                ap += ap_tmp
            CMC = CMC.astype('float')
            CMC = CMC/query_size  # average CMC
            mAP = ap/query_size
            self.reidmetric = (CMC, mAP)
            return self.reidmetric

def evaluate_oneitem(qf, ql, qc, gf, gl, gc):
    query = qf
    score = np.dot(gf, query)
    # predict index
    index = np.argsort(score)  # from small to large
    index = index[::-1]
    #index = index[0:2000]
    # good index
    query_index = np.argwhere(gl == ql)
    camera_index = np.argwhere(gc == qc)
    good_index = np.setdiff1d(query_index, camera_index, assume_unique=True)
    junk_index1 = np.argwhere(gl == -1)
    junk_index2 = np.intersect1d(query_index, camera_index)
    junk_index = np.append(junk_index2, junk_index1)  # .flatten())
    CMC_tmp = compute_mAP_(index, good_index, junk_index)
    return CMC_tmp

# compute map
def compute_mAP_(index, good_index, junk_index):
    ap = 0
    cmc = np.zeros(len(index))
    if good_index.size == 0:   # if empty
        cmc[0] = -1
        return ap, cmc
    # remove junk_index
    mask = np.in1d(index, junk_index, invert=True)
    index = index[mask]
    # find good_index index
    ngood = len(good_index)
    mask = np.in1d(index, good_index)
    rows_good = np.argwhere(mask == True)
    rows_good = rows_good.flatten()

    cmc[rows_good[0]:] = 1
    for i in range(ngood):
        d_recall = 1.0/ngood
        precision = (i+1)*1.0/(rows_good[i]+1)
        if rows_good[i] != 0:
            old_precision = i*1.0/rows_good[i]
        else:
            old_precision = 1.0
        ap = ap + d_recall*(old_precision + precision)/2
    return ap, cmc
